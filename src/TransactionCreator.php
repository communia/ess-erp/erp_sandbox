<?php

namespace Drupal\erp_sandbox;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\erp_transaction\Entity\Transaction;
use Drupal\node\Entity\Node;
use Drupal\commerce_price\Price;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\EntityInterface;

//Manual base
use Drupal\commerce_store\StoreCreationTrait;

//use Drupal\Tests\commerce_cart\Traits\CartManagerTestTrait;

/**
 * Tests the transaction entity.
 *
 * @coversDefaultClass \Drupal\erp_transaction\Entity\Transaction
 *
 * @group commerce
 */
class TransactionCreator {
  //use CartManagerTestTrait;
  use StoreCreationTrait;
  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The default store.
   *
   * @var \Drupal\commerce_store\Entity\StoreInterface
   */
  protected $store;

  public function __construct() {
    $this->store = $this->createStore('Default store', 'admin@example.com');
    \Drupal::entityTypeManager()->getStorage('commerce_store')->markAsDefault($this->store);

    $this->setUp();
    $this->testTransactionOperations();
  }

  protected function randomString($length = 10){
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
  }
  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->user = user_load(1);

    // First it creates a Basic product, then new product based on composition of basics.
    $basics = $this->createBasicProductVariation();
    $complexes = $this->createCompositionProductVariation();

    // Create a product design of the complex product (variation).
    $product_design = $this->createProductDesign([$basics['variation']],$complexes['variation']->id());

    // Creates a production order vased on product design recipe
    $order_items_in_order = [];
    foreach($product_design->bill_of_materials as $delta => $order_item_from_pd){
      $order_item = OrderItem::create([
        'title' => 'OrderItem in production order #' . $delta,
        'type' => 'erp_production_order',
        'quantity' => (string)($order_item_from_pd->entity->quantity->value * 10), // Product design quantity multiplicated by the order planned_quantity
        'purchased_entity' => $order_item_from_pd->entity->getPurchasedEntity(),
        'unit_price' => new Price('30.00', 'USD')
      ]);
      $order_item->save();
      $order_item = $this->reloadEntity($order_item);
      $order_items_in_order[] = $order_item;
    }
    $order = Order::create([
      'type' => 'erp_production_order',
      'uid' => $this->user->id(),
      'store_id' => $this->store->id(),
      'order_number' => 'production order #' . $this->randomString(5),
      'order_items' => $order_items_in_order,
      'planned_quantity' => 10.0,
      'product_design' => $product_design->id(),
      'target_product' => $complexes['variation']->id()
    ]);
    $order->save();
    $this->order = $order;
    $this->order = $this->reloadEntity($order);

  }

  /**
   * Creates a composition of basic products product and its product variation.
   *
   * @return array
   *  with keys "product" and its "product_variation".
   */
  protected function createCompositionProductVariation() {
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = Product::create([
      'type' => 'erp_resource',
      'title' => 'Basket of apples',
      'stores' => [$this->store->id()],
    ]);
    $product->save();
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = ProductVariation::create([
      'type' => 'erp_resource',
      'product_id' => $product->id(),
			'minimum_stock_level' => 5,
      'quantity' => 100,
    ]);
    $variation->save();
    $product = $this->reloadEntity($product);
    $variation = $this->reloadEntity($variation);
    return ["product" => $product, "variation" => $variation];

 }

  /**
   * Creates a basic product and its product variation.
   *
   * @return array
   *  with keys "product" and its "product_variation".
   */
  protected function createBasicProductVariation() {
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = Product::create([
      'type' => 'erp_resource',
      'title' => 'Apple',
      'variations' => [],
      'stores' => [$this->store->id()],
    ]);
    $product->save();
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = ProductVariation::create([
      'type' => 'erp_resource',
      'product_id' => $product->id(),
			'minimum_stock_level' => 5,
      'quantity' => 500,
      'price' => new Price('30.00', 'USD'),
    ]);
    $variation->save();
    $product = $this->reloadEntity($product);
    $variation = $this->reloadEntity($variation);
    return ["product" => $product, "variation" => $variation];

 }

  /**
   * create Product Design.
   *
   * @return NodeInterface
   *   The created product_design.
   */
  protected function createProductDesign($variations, $designed_resource_id) {
    $order_items = [];
    foreach ($variations as $delta => $variation){
      $order_item = OrderItem::create([
        'title' => 'Product design OI#' . $delta,
        'type' => 'erp_production_order',
        'quantity' => 10,
        'purchased_entity' => $variation,
        'unit_price' => new Price('31.00', 'USD'),
      ]);
      $order_item->save();
      $order_item = $this->reloadEntity($order_item);
      $order_items[] = $order_item;
    }

    /** @var \Drupal\core\Entity\NodeInterface $product_design */
    $product_design = Node::create([
      'title' => 'Apple basket design',
      'type' => 'erp_product_design',
      'designed_resource' => $designed_resource_id,
      'bill_of_materials' => $order_items,
    ]);
    $product_design->save();
    $product_design = $this->reloadEntity($product_design);
    return $product_design;
  }

  /**
   * Tests the order integration (total_paid field).
   *
   * @covers ::postSave
   * @covers ::postDelete
   */
  public function testTransactionOperations() {

    $pre_create_stock_quantity = $this->order->target_product->entity->quantity->value;
    $pre_create_processed = [];
    foreach($this->order->order_items as $delta => $order_item_from_order){
      $pre_create_processed[] = $order_item_from_order->entity->processed->value;
      // must be
    }
    $pre_create_executed = $this->order->executed_quantity->value;

    $increase_in_target = 1;
    $waste_in_bom = 10;


    /** Creates a transaction that substracts basic resource coming from order from stock
     *  And increases the stock of complex resource from order.
     */
    $order_items_in_transaction = [];
    foreach($this->order->order_items as $delta => $order_item_from_order){
      $order_item_substract = OrderItem::create([
        'title' => 'OrderItem BOM in transaction order #' . $delta,
        'type' => 'erp_default',
        'quantity' => $waste_in_bom,
        // $order_item_from_order->entity->quantity->value to process  everything
        'purchased_entity' => $order_item_from_order->entity->getPurchasedEntity(),
        'substract' => TRUE,
        'unit_price' => new Price('30.00', 'USD'),
      ]);
      $order_item_substract->save();
      $order_item_substract = $this->reloadEntity($order_item_substract);
      $order_items_in_transaction[] = $order_item_substract->id();
    }
    $order_item_create = OrderItem::create([
        'title' => 'OrderItem target in transaction order #' . $delta,
        'type' => 'erp_default',
        'quantity' => $increase_in_target,
        'purchased_entity' => $this->order->target_product->entity,
        'substract' => FALSE,
        'unit_price' => new Price('30.00', 'USD'),
      ]);
    $order_item_create->save();
    $order_item_create = $this->reloadEntity($order_item_create);
    $order_items_in_transaction[] = $order_item_create->id();

    $transaction = Transaction::create([
      'type' => 'production_note',
      'order_id' => $this->order->id(),
      'title' => 'transaction_prnote',
      'resource' => $order_items_in_transaction,
    ]);
    $transaction->save();
    $transaction = $this->reloadEntity($transaction);
    $order = $this->reloadEntity($this->order);
    $post_create_processed = [];
    foreach($order->order_items as $delta => $order_item_from_order){
      $post_create_processed[] = $order_item_from_order->entity->processed->value;
    }
  }

  protected function reloadEntity(EntityInterface $entity) {
    $controller = \Drupal::entityManager()
      ->getStorage($entity
      ->getEntityTypeId());
    $controller
      ->resetCache(array(
        $entity
        ->id(),
      ));
    return $controller
      ->load($entity
      ->id());
  }
}
