<?php

namespace Drupal\erp_sandbox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\erp_sandbox\TransactionCreator;


/**
 * Class CreateDemoContentForm.
 */
class CreateDemoContentForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'erp_sandbox.createdemocontent',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_demo_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('erp_sandbox.createdemocontent');
    $form = parent::buildForm($form, $form_state);
    $form['info'] = [
      '#markup' => 'Pressing save button will create demo content.'
    ];
    $form['actions']['submit']['#value'] = $this->t('Create ERP demo content');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('erp_sandbox.createdemocontent')
      ->save();
    new TransactionCreator();
  }

}
